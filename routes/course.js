const router = require('express').Router();
const { authenticate } = require('../middlewares/authenticate');
const Course = require('../models/course');
const Chapter = require('../models/chapter');
const Lecture = require('../models/lecture');

const _ = require('lodash');

router.post('/addCourse', authenticate, (req, res) => {
    Course.addCourse(req.body, req.user)
        .then(addedCourse => res.json(addedCourse))
        .catch(err => res.status(400).json(_.pick(err, ['name', 'message'])));
});

router.post('/addChapterToCourse', (req, res) => {
    Chapter.find({}, { _id: 1 })
        .then(foundChapters => {
            foundChapters = foundChapters.map(el => el._id);
            Course.findOne({})
                .then(foundCourse => {
                    foundCourse.chapters.push(...foundChapters);
                    foundCourse.save()
                        .then(addedCourse => res.json(addedCourse));

                })
        })
        .catch(err => res.status(400).json(_.pick(err, ['name', 'message'])));
});

router.get('/getCourse', (req, res) => {
    Course.getOneCourse({ _id: req.query.id })
        .then(foundCourse => res.json(foundCourse))
        .catch(err => res.status(400).json(_.pick(err, ['name', 'message'])));

});


module.exports = router;