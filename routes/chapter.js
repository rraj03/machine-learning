const router = require('express').Router();
const { authenticate } = require('../middlewares/authenticate');
const Course = require('../models/course');
const Chapter = require('../models/chapter');
const Lecture = require('../models/lecture');
const Quiz = require('../models/quiz');
const _ = require('lodash');

router.post('/addChapter', authenticate, (req, res) => {
    let lectureIds = [];
    let createdChapter1;
    Chapter.addChapter(req.body)
        .then(createdChapter => {
            createdChapter1 = createdChapter;
            Lecture.find({}, { _id: 1 })
                .then(foundlectureIds => {
                    lectureIds = foundlectureIds.map(el => el._id);
                    Quiz.find({}, { _id: 1 })
                        .then(foundQuizIds => {
                            foundQuizIds = foundQuizIds.map(el => el._id);
                            createdChapter1.lectures.push(...lectureIds);
                            createdChapter1.quizs.push(...foundQuizIds);
                            createdChapter1.author = req.user._id;
                            createdChapter1.save()
                                .then(savedChapters => res.json(createdChapter1));

                        })

                })
        })
        .catch(err => res.status(400).json(_.pick(err, ['name', 'message'])))
});


router.get('/getChapters', (req, res) => {
    Chapter.getChapters()
        .then(foundChapters => res.json(foundChapters))
        .catch(err => res.status(400).json(_.pick(err, ['name', 'message'])))

})


module.exports = router;
