const router = require('express').Router();
const User = require('../models/user');
const _ = require('lodash');


router.post('/register', (req, res) => {
    User.addUser(req.body)
        .then(createdUser => res.json(createdUser))
        .catch(err => res.status(400).json(_.pick(err, ['name', 'message'])));
});
module.exports = router;
