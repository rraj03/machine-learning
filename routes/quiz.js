const router = require('express').Router();
const { authenticate } = require('../middlewares/authenticate');
const Course = require('../models/course');
const Chapter = require('../models/chapter');
const Lecture = require('../models/lecture');
const Quiz = require('../models/quiz');
const _ = require('lodash');

router.post('/addQuizs', (req, res) => {
    Quiz.addQuizs(req.body)
        .then(createdQuiz => res.json(createdQuiz))
        .catch(err => res.status(400).json(_.pick(err, ['name', 'message'])))
});



module.exports = router;