const router = require('express').Router();
const { authenticate } = require('../middlewares/authenticate');
const Course = require('../models/course');
const Chapter = require('../models/chapter');
const Lecture = require('../models/lecture');
const Quiz = require('../models/quiz');

const _ = require('lodash');

router.post('/addLectures', (req, res) => {
    Lecture.addLectures(req.body)
        .then(createdLectures => res.json(createdLectures))
        .catch(err => res.status(400).json(_.pick(err, ['name', 'message'])))
});



module.exports = router;
