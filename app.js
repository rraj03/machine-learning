const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const config = require('./config/env');

const courseRoutes = require('./routes/course');
const quizRoutes = require('./routes/quiz');
const lectureRoutes = require('./routes/lecture');
const chapterRoutes = require('./routes/chapter');

const userRoutes = require('./routes/user');
const { mongoose } = require('./config/db');
app.use(bodyParser.json({ limit: '100mb' }));

app.use(express.static(__dirname + '/public'));



app.use('/course', courseRoutes);
app.use('/quiz', quizRoutes);
app.use('/lecture', lectureRoutes);
app.use('/chapter', chapterRoutes);

app.use('/user', userRoutes);

app.listen(config.PORT, () => {
    console.log(`Server is up ${config.PORT}`);
});
