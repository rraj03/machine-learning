const User = require('../models/user');
module.exports.authenticate = function (req, res, next) {
    User.findOne({}).then(foundUser => {
        req.user = foundUser;
        next();
    });
}