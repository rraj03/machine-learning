const { mongoose } = require('../config/db');
const Lecture = require('./lecture');
const Quiz = require('./quiz');
const User = require('./user');

const chapterSchema = mongoose.Schema({
    title: {
        type: String,
        trim: true,
        minlength: 1,
        required: true
    },
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    lectures: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Lecture'
        }
    ],
    quizs: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Quiz'
        }
    ]

}, {
        timestamps: true
    })

chapterSchema.statics.addChapter = function (chapter) {
    return this.create(chapter);
}

chapterSchema.statics.getChapters = function () {
    return this.find({}).populate(['lectures', 'quizs']);
}
const Chapter = mongoose.model('Chapter', chapterSchema);

module.exports = Chapter;