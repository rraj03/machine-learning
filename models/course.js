const { mongoose } = require('../config/db');
const User = require('./user');
const Chapter = require('./chapter');
const _ = require('lodash');

const courseSchema = mongoose.Schema({
    image: {
        type: String,
        required: true,
        minlength: 1,
        trim: true
    },
    chapters: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Chapter'
        }
    ],
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    title: {
        type: String,
        required: true,
        minlength: 1,
        trim: true
    },
    ratingData: [
        {
            rating: {
                type: Number
            },
            rater: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            },
            date: {
                type: Date,
                default: Date.now
            },
        }
    ],
    subscribers: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }
    ]

}, {
        timestamps: true
    });



courseSchema.statics.addCourse = function (course, user) {
    return this.findOne(_.pick(course, ['title']))
        .then(foundCource => {
            if (foundCource)
                return Promise.reject({ name: 'DuplicateKey', message: "Course title is not available" });
            var newCourse = new Course(_.pick(course, ['image', 'title']));
            newCourse.author = user._id;
            return newCourse.save();
        })
}

courseSchema.statics.getCourses = function (query) {
    return this.find(query);
}

courseSchema.statics.getOneCourse = function (query) {
    return this.findOne(query).populate({ path: 'chapters', populate: ['lectures', 'quizs'] });
}
const Course = mongoose.model('Course', courseSchema);

module.exports = Course;