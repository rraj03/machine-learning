const { mongoose } = require('../config/db');

const quizSchema = mongoose.Schema({
    title: {
        type: String,
        trim: true,
        minlength: 1,
        required: true
    },
    about: {
        type: String
    },
    resources: {
        type: String
    }
}, { timestamp: true });

quizSchema.statics.addQuizs = function (quizs) {
    return this.insertMany(quizs);
}

const Quiz = mongoose.model('Quiz', quizSchema);

module.exports = Quiz;