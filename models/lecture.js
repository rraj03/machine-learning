const { mongoose } = require('../config/db');
const User = require('./user');

const lectureSchema = mongoose.Schema({
    title: {
        type: String,
        trim: true,
        minlength: 1,
        required: true
    },
    video: {
        type: String
    },
    about: {
        type: String
    },
    resources: {
        type: String
    }
}, { timestamp: true });

lectureSchema.statics.addLectures = function (lectures) {
    return this.insertMany(lectures);
}

const Lecture = mongoose.model('Lecture', lectureSchema);

module.exports = Lecture;