const { mongoose } = require('../config/db');
const validator = require('validator');
const _ = require('lodash');

const userSchema = mongoose.Schema({
    firstName: {
        type: String,
        required: true,
        trim: true
    },
    lastName: {
        type: String,
        trim: true

    },
    email: {
        type: String,
        required: true,
        trim: true,
        validate: {
            validator: validator.isEmail,
            message: '{VALUE} is not a valid email'
        }

    },
    password: {
        type: String,
        required: true,
        minlength: 6
    },
    contactNumber: {
        type: String,
        trim: true

    }

}, {
        timestamps: true
    });

userSchema.statics.addUser = function (userData) {
    return this.findOne(_.pick(userData, ['email']))
        .then(foundUser => {
            if (foundUser)
                return Promise.reject({ name: 'DuplicateKey', message: "An account with this email is already registered" });
            return this.create(_.pick(userData, ['firstName', 'lastName', 'email', 'password', 'contactNumber']));
        })
}

const User = mongoose.model('User', userSchema);

module.exports = User;