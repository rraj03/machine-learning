const mongoose = require('mongoose');
const config = require('./env');
mongoose.Promise = global.Promise;

const options = {
    autoIndex: false,
    reconnectTries: 10,
    reconnectInterval: 500,
    poolSize: 10,
    bufferMaxEntries: 0,
    useNewUrlParser: true
};

mongoose.connect(config.MONGODB_URI, options).then(
    () => {
        console.log('Connected to database', config.MONGODB_URI);
    },
    err => {
        console.log('Error connecting to database: ', err);
    }
);

module.exports = { mongoose };