const dotenv = require('dotenv');

let result = dotenv.config();
if (result.error) {
    console.log(result.error);
    console.log('No .env file found!');
}

const finalConfig = process.env;

module.exports = finalConfig;